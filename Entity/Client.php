<?php

declare(strict_types=1);

namespace lst\ClientsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\MediaBundle\Entity\File;

/**
 * Clients Test Entity
 * @ORM\Table(name="clients")
 * @ORM\Entity(repositoryClass="lst\ClientsBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * @ORM\OneToOne(targetEntity="lst\MediaBundle\Entity\File", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    protected $image;

    /**
     * @var string
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $link;

    public function __construct(string $name, string $description, File $image, string $link)
    {
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return File|null
     */
    public function getImage() : ?File
    {
        return $this->image;
    }

    /**
     * @param File|null $image
     */
    public function setLogo(?File $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string|null
     */
    public function getLink() : ?string
    {
        return $this->link;
    }

    /**
     * @param string|null $link
     */
    public function setLink(?string $link): void
    {
        $this->link = $link;
    }
}
