<?php

declare(strict_types=1);

namespace lst\ClientsBundle\Controller;

use lst\ClientsBundle\Entity\Client;
use lst\ClientsBundle\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ClientsController extends AbstractController
{
    /**
     * @var ClientRepository
     */
    private $repostitory;
    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    public function __construct(ClientRepository $repository, NormalizerInterface $normalizer)
    {
        $this->repostitory = $repository;
        $this->normalizer = $normalizer;
    }

    /**
     * @Route("/clients", name="clients.list", methods={"GET"})
     */
    public function clientsList() : JsonResponse
    {
        $data = $this->repostitory->findAll();
        $clients = $this->normalizer->normalize($data);

        return new JsonResponse($clients);
    }

    /**
     * @Route("/clients/{id}", name="clients.get", methods={"GET"}, requirements={"\d+"})
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function clientsGet(Client $client) : JsonResponse
    {
        return new JsonResponse($this->normalizer->normalize($client));
    }
}
