<?php

namespace lst\ClientsBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use lst\ClientsBundle\Entity\Client;
use lst\MediaBundle\Entity\File;

class ClientsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $image = new File('GHE', 'https://lebedev-studio.com/photos/c1cc07ff01ecff4d8472c4acf9540f1e_200-200', '.jpg');
        $client = new Client('GHE', 'Best hotel around the world', $image, 'https://ghe.com');
        $manager->persist($client);

        $image = new File('borhe', 'https://lebedev-studio.com/photos/a19f1a59b95ddabda84d7dd0df2be06b_200-200', 'jpg');
        $client = new Client('bohre', 'Annual Cutters', $image, 'https://bohre.com');
        $manager->persist($client);

        $image = new File('Mad Max', 'https://lebedev-studio.com/photos/dbd870a3239e3a5d8156d94fb3c0b4e5_200-200', 'jpg');
        $client = new Client('Mad Max', 'We drink', $image, 'https://mad-max.com');
        $manager->persist($client);

        $image = new File('Северный путь', 'https://lebedev-studio.com/photos/221dee9ed7b6089608a31c936ec04b51_200-200', 'jpg');
        $client = new Client('Северный путь', 'Путь в никуда', $image, 'https://northen-path.com');
        $manager->persist($client);

        $manager->flush();
    }
}
